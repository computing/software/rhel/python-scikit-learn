# Licensed under a 3-clause BSD style license - see README.rst
"""
Handle loading joblib package from system or from the bundled copy

"""
import sys

_dest_root = 'sklearn.externals.joblib'
# Trying to load alternate packages
sys.modules[_dest_root] = None

# We have removed everything we already imported
# Importing again
import sys 

_system_package = False
_dest_root = 'sklearn.externals.joblib'

try:
    import joblib
    _system_package = True
except ImportError:
    _system_package = False

if _system_package:
    # Check version
    from distutils.version import StrictVersion
    _valid_version = False
    if StrictVersion(joblib.__version__) >= StrictVersion('0.8.0'):
        _valid_version = True

    if _valid_version:
        joblib.system_package = True
        joblib.bundled_package = False
        sys.modules[_dest_root] = joblib
    else:
        _system_package = False

if not _system_package:
    import sklearn.externals.bundled.joblib as joblib
    joblib.system_package = False
    joblib.bundled_package = True
    sys.modules[_dest_root] = joblib

