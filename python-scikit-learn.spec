%global srcname scikit-learn
%global sum Machine learning in Python

%global common_description                                                   \
Scikit-learn integrates machine learning algorithms in the tightly-knit      \
scientific Python world, building upon numpy, scipy, and matplotlib.         \
As a machine-learning module, it provides versatile tools for data mining    \
and analysis in any field of science and engineering. It strives to be       \
simple and efficient, accessible to everybody, and reusable                  \
in various contexts.

Name: python-scikit-learn
Version: 0.19.1
Release: 2.2%{?dist}
Summary: %{sum}
License: BSD

URL: http://scikit-learn.org/
Source0: https://pypi.io/packages/source/s/scikit-learn/%{srcname}-%{version}.tar.gz
Source1: system-six.py
Source2: system-joblib.py
Patch0: sklearn-unbundle-joblib.patch
Patch1: blas-name.patch

BuildRequires: python%{python3_pkgversion}-devel 
BuildRequires: atlas-devel blas-devel

# we don't want to provide private python extension libs
%global __provides_exclude_from ^(%{python2_sitearch}|%{python3_sitearch}|%{python3_other_sitearch})/.*\\.so$

%description
%{common_description}

%package -n python%{python3_pkgversion}-%{srcname}
Summary: %{sum}
BuildRequires: python%{python3_pkgversion}-nose
BuildRequires: python%{python3_pkgversion}-numpy python%{python3_pkgversion}-scipy python%{python3_pkgversion}-matplotlib
BuildRequires: python%{python3_pkgversion}-joblib
BuildRequires: python%{python3_pkgversion}-six >= 1.4.1
BuildRequires: python%{python3_pkgversion}-Cython python%{python3_pkgversion}-pillow
Requires: python%{python3_pkgversion}-numpy python%{python3_pkgversion}-scipy python%{python3_pkgversion}-joblib

%{?python_provide:%python_provide python%{python3_pkgversion}-%{srcname}}
%{?python_provide:%python_provide python%{python3_pkgversion}-sklearn}

%description -n python%{python3_pkgversion}-%{srcname}
%{common_description}


%prep
%autosetup -n %{srcname}-%{version} -p1

# Code to load system six and joblib at runtime
mkdir sklearn/externals/bundled
touch sklearn/externals/bundled/__init__.py
mv sklearn/externals/six.py sklearn/externals/bundled
mv sklearn/externals/joblib sklearn/externals/bundled
mv sklearn/externals/test_externals_setup.py sklearn/externals/bundled
cp %{SOURCE1} sklearn/externals/six.py
cp %{SOURCE2} sklearn/externals/joblib.py

# Remove the source code of the bundled blas library
rm -rf sklearn/src/cblas/*

#chmod -x examples/decomposition/plot_pca_vs_fa_model_selection.py
#sed -i -e "1d" examples/decomposition/plot_pca_vs_fa_model_selection.py

#find -name '*.py' | xargs sed -i '1s|^#!python|#!%{__python2}|'

%build
%py3_build

%install
%py3_install

#%check
#pushd %{buildroot}/%{python3_sitearch}
#nosetests-%{python3_version} -x sklearn
#popd

%files -n python%{python3_pkgversion}-%{srcname}
%doc examples/ AUTHORS.rst README.rst
%license COPYING
%{python3_sitearch}/sklearn
%{python3_sitearch}/scikit_learn-*.egg-info

%changelog
* Sat Oct 5 2019 Michael Thomas <michael.thomas@LIGO.ORG> - 0.19.1-2.2
- Drop python3.4, python2 support

* Thu Nov 29 2018 Michael Thomas <michael.thomas@LIGO.ORG> - 0.19.1-2.1
- Add python3.6 support

* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 0.19.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Fri Nov 03 2017 Colin B. Macdonald <cbm@m.fsf.org> - 0.19.1-1
- New upstream (0.19.1)

* Thu Aug 03 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.18.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.18.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.18.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Fri Jan 27 2017 Sergio Pascual <sergiopr@fedoraproject.org> - 0.18.1-1
- New upstream (0.18.1)

* Mon Dec 19 2016 Miro Hrončok <mhroncok@redhat.com> - 0.18-3
- Rebuild for Python 3.6

* Sun Nov  6 2016 Orion Poplawski <orion@cora.nwra.com> - 0.18-2
- Rebuild for ppc64

* Thu Oct 27 2016 Sergio Pascual <sergiopr@fedoraproject.org> - 0.18-1
- New upstream (0.18)
- Updatd patch blas-name
- Removed patch sklearn-np11 (already in upstream)

* Sat Oct 15 2016 Peter Robinson <pbrobinson@fedoraproject.org> - 0.17.1-6
- rebuilt for matplotlib-2.0.0

* Tue Jul 19 2016 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.17.1-5
- https://fedoraproject.org/wiki/Changes/Automatic_Provides_for_Python_RPM_Packages

* Wed Mar 30 2016 Björn Esser <fedora@besser82.io> - 0.17.1-4
- add Provides for python(2|3)-sklearn

* Wed Mar 30 2016 Björn Esser <fedora@besser82.io> - 0.17.1-3
- add proper Provides and Obsoletes

* Wed Mar 30 2016 Sergio Pascual <sergiopr@fedoraproject.org> - 0.17.1-2.1
- Skip tests for the moment

* Tue Mar 29 2016 Sergio Pascual <sergiopr@fedoraproject.org> - 0.17.1-2
- New upstream (0.17.1)
- Provide python2-scikit-learn
- Add patch for numpy1.11
- New style macros

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.17-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Wed Nov 25 2015 Zbigniew Jędrzejewski-Szmek <zbyszek@in.waw.pl> - 0.17-1
- Update to latest version
- Force linking to atlas

* Tue Nov 10 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.16.1-3
- Rebuilt for https://fedoraproject.org/wiki/Changes/python3.5

* Thu Jun 18 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.16.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Thu Apr 09 2015 Sergio Pascual <sergiopr@fedoraproject.org> - 0.16.1-1
- New upstream (0.16.1), bugfix

* Thu Apr 09 2015 Sergio Pascual <sergiopr@fedoraproject.org> - 0.16.0-2
- Readd provides filter
- Increase joblib minimum version
- New upstream (0.16.0)

* Tue Sep 16 2014 Sergio Pascual <sergiopr@fedoraproject.org> - 0.15.2-2
- Remove patch for broken test (fixed in scipy 0.14.1)

* Tue Sep 16 2014 Sergio Pascual <sergiopr@fedoraproject.org> - 0.15.2-1
- New upstream (0.15.2), bugfix

* Sun Aug 17 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.15.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Sat Aug 02 2014 Sergio Pascual <sergiopr@fedoraproject.org> - 0.15.1-1
- New upstream (0.15.1), bugfix

* Tue Jul 15 2014 Sergio Pascual <sergiopr@fedoraproject.org> - 0.15.0-1
- New upstream (0.15.0), final

* Wed Jul 02 2014 Sergio Pascual <sergiopr@fedoraproject.org> - 0.15.0-0.5.b2
- New upstream (0.15.0b2), beta release

* Tue Jun 24 2014 Sergio Pascual <sergiopr@fedoraproject.org> - 0.15.0-0.4.b1
- Add COPYING to docs
- Spec cleanup

* Mon Jun 23 2014 Sergio Pascual <sergiopr@fedoraproject.org> - 0.15.0-0.3.b1
- New upstream (0.15.0b1), beta release
- Add tarball
- Disable tests for the moment

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.14.1-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Mon Jun 02 2014 Bohuslav Kabrda <bkabrda@redhat.com> - 0.14.1-8
- Rebuilt for https://fedoraproject.org/wiki/Changes/Python_3.4

* Mon Jun 02 2014 Sergio Pascual <sergiopr@fedoraproject.org> - 0.14.1-7
- Rerun Cython3 on broken files
- Disable tests for the moment

* Thu May 29 2014 Björn Esser <bjoern.esser@gmail.com> - 0.14.1-6
- rebuilt for Python3 3.4

* Wed Jan 15 2014 Sergio Pascual <sergiopr@fedoraproject.org> - 0.14.1-5
- Enable checks
- Regenerate C files from patched cython code
- Use python2 style macros

* Sat Oct 26 2013 Björn Esser <bjoern.esser@gmail.com> - 0.14.1-4
- rebuilt for atlas-3.10.1-7

* Mon Sep 16 2013 Sergio Pascual <sergiopr@fedoraproject.org> - 0.14.1-3
- Unbundle six

* Wed Sep 11 2013 Sergio Pascual <sergiopr@fedoraproject.org> - 0.14.1-2
- Update cblas patch
- Update EVR to build with new numpy (1.8.0-0.3b2)

* Wed Aug 28 2013 Sergio Pascual <sergiopr@fedoraproject.org> - 0.14.1-1
- New upstream source (0.14.1)
- Add python3 support
- Unbundle joblib and cblas

* Wed Jul 10 2013 Sergio Pascual <sergiopr@fedoraproject.org> - 0.13.1-3
- Reorder buildrequires and requires
- Dropped doc, it does not build

* Tue Jun 25 2013 Sergio Pascual <sergiopr@fedoraproject.org> - 0.13.1-2
- Changed package name
- Tests do not need recompile

* Thu Apr 18 2013 Sergio Pascual <sergiopr@fedoraproject.org> - 0.13.1-1
- Initial spec file
